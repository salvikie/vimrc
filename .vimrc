" tab 键设置
" noexpandtab: tab转化成space 实际还是以\t写入
" shitwidth: tab宽度
" tabstop: 读到\t解释为space个数
" softtablestop: 操作 Backspace 的時候，4 個空白會被當作一個 tab 刪除
" autoindent: 自动缩进及每行的缩进值与上一行相等
set noexpandtab shiftwidth=4 tabstop=4 softtabstop=4 autoindent

"设置显示行号
set nu 
" 设置在命令行界面最下面显示当前模式等
set showmode 
" 在右下角显示光标所在的行数等信息
set ruler 
"设置每次单击Enter键后，光标移动到下一行时与上一行的起始字符对齐
set autoindent 
" 即设置语法检测，当编辑C或者Shell脚本时，关键字会用特殊颜色显示粗体文本
syntax on 

" 从一个vim yy 到另一个vim pp 数据拷贝粘贴
set mouse=v
set clipboard=unnamed
